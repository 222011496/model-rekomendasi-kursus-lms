from fastapi import FastAPI
import pandas as pd
## IMPORT FILE YG MAU DIEKSEKUSI
#import lda

app = FastAPI()
weights = pd.read_csv("D:\Documents\.kuliah\.skrispi\execution\output\eq recommendation new.csv")

@app.get("/")
def read_root():
    return {"Hello": "World"}

@app.get("/{user_id}")
def recommendation(user_id: int):
    result = []
    if user_id in weights['userid'].tolist():
        result = weights[weights['userid'] == user_id].sort_values(by='final_weight', ascending=False).head(10)['courseid'].tolist()
        status = 200
        msg = 'Berhasil'
    else :
        status = 404
        msg = 'User tidak ditemukan'
    
    json_data = {
        'userid': user_id,
        'courseids': result,
        'status': status,
        'message': msg
    }

    return json_data
